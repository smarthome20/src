import RPi.GPIO as GPIO
import time

RedLed = 27 
GreenLed = 17
PushButton = 26
Buzzer = 4


# Pin Setup:

GPIO.setmode(GPIO.BCM)
GPIO.setup(RedLed, GPIO.OUT)
GPIO.setup(GreenLed, GPIO.OUT)
GPIO.setup(Buzzer, GPIO.OUT)
GPIO.setup(PushButton, GPIO.IN)


# Initial state for LED:
GPIO.output(RedLed, GPIO.LOW)
GPIO.output(GreenLed, GPIO.HIGH)
print("Simple Python GPIO Start! Press CTRL+C to exit")

def on() :
     GPIO.output(Buzzer, GPIO.LOW)

def off() : 
     GPIO.output(Buzzer, GPIO.HIGH)

def beep(x) :
     on()
     time.sleep(x)
     off()
     time.sleep(x)
try:
   while 1:
      if GPIO.input(PushButton):
        GPIO.output(RedLed, GPIO.HIGH)
        GPIO.output(GreenLed, GPIO.LOW)
        beep(0.5)
        
      else:
        GPIO.output(RedLed, GPIO.LOW)
        GPIO.output(GreenLed, GPIO.HIGH)

except KeyboardInterrupt: # If CTRL+C is pressed, exit and clean
GPIO.cleanup() # cleanup all GPIO