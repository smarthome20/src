#include "simpletools.h"                      // Include simple tools
#include "dht22.h"
int main(){
 cog_run(tempNhumid,128); //can use 8cogs 
 cog_run(motionNalarm,128);
}

void motionNalarm(){
while(1)
  {
    int motion = input(5);    //motion #5
    int door = input(7);      //door #7
             //green #1, red #0, buzzer #4
   
    if(motion ==1 && door ==1)
    {
      print("%cMotion Detected! %c \n", HOME, CLREOL);
      low(1);
      high(0);
      freqout(4, 1000, 3000);
      pause(250);
     }
     
   else if(motion ==1 && door ==0)
   
     {
      print("%cMotion Detected! and Door Breached! %c \n", HOME, CLREOL);
      low(1);
      high(0);
      freqout(4, 1000, 3000);
      pause(250);
      }
      
    else if(motion == 0 && door == 0)
      {
        print("%cDoor Breached!  %c \n", HOME, CLREOL);
        low(1);
        high(0);
        freqout(4, 1000, 3000);
        pause(250);
      }
   
    else
     {
       print("%cAll good  %c \n", HOME, CLREOL); 
       high(1);
       low(0);
       pause(250);     
    }  
    
  }  
}

void tempNhumid()                                    // Main function
{
  int temp, humid, pin;
  
  while(1)
  {
    pin = dht22_read(15);    //humid pin #15
    temp = dht22_getTemp(FAHRENHEIT);
    humid =dht22_getHumidity();
    
    print("temp = %.1f, humid =%.1f\n",temp/10.0, humid/10.0);
    pause(100);
    
    if(temp > 75.0){
         print("Fan is turned on!");  //fan pin #14
  high(14);
 }
    else low(14);
  }  
}
