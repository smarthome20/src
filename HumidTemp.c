
#include "simpletools.h"                      // Include simple tools
#include "dht22.h"

int main()                                    // Main function
{
  int temp, humid, pin;
  
  while(1)
  {
    pin = dht22_read(15); //pin #5
    temp = dht22_getTemp(FAHRENHEIT);
    humid =dht22_getHumidity();
    
    print("temp = %.1f, humid =%.1f\n",temp/10.0, humid/10.0);
    pause(250);
    
  }  
}
